module ecchat_desktop/go

go 1.15

require (
	github.com/Xuanwo/go-locale v1.0.0 // indirect
	github.com/go-flutter-desktop/go-flutter v0.43.0
	github.com/go-flutter-desktop/plugins/image_picker v0.1.5
	github.com/go-flutter-desktop/plugins/path_provider v0.4.0
	github.com/go-gl/gl v0.0.0-20210315015930-ae072cafe09d // indirect
	github.com/go-gl/glfw v0.0.0-20201108214237-06ea97f0c265 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210311203641-62640a716d48 // indirect
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/miguelpruivo/flutter_file_picker/go v0.0.0-20210108225058-9500baac976b
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	golang.org/x/text v0.3.6 // indirect
)
