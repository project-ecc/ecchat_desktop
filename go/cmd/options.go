package main

import (
	"github.com/go-flutter-desktop/go-flutter"
	"github.com/go-flutter-desktop/plugins/path_provider"
	file_picker "github.com/miguelpruivo/flutter_file_picker/go"
)

var options = []flutter.Option{
	flutter.WindowInitialDimensions(800, 1280),
	flutter.AddPlugin(&path_provider.PathProviderPlugin{
		VendorName:      "Project-ecc",
		ApplicationName: "ecchat",
	}),
	flutter.AddPlugin(&file_picker.FilePickerPlugin{}),
}
