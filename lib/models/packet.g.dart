//@dart=2.7
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'packet.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PacketAdapter extends TypeAdapter<Packet> {
  @override
  final int typeId = 4;

  @override
  Packet read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Packet(
      id: fields[0] as String,
      ver: fields[1] as String,
      to: fields[2] as String,
      from: fields[3] as String,
      meth: fields[4] as String,
      data: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Packet obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.ver)
      ..writeByte(2)
      ..write(obj.to)
      ..writeByte(3)
      ..write(obj.from)
      ..writeByte(4)
      ..write(obj.meth)
      ..writeByte(5)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PacketAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
