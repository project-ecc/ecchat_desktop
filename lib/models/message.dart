//@dart=2.7
import 'dart:convert';

import 'package:hive/hive.dart';
part 'message.g.dart';

@HiveType(typeId: 2)
class Message extends HiveObject implements Comparable {
  @HiveField(0)
  String id;
  @HiveField(1)
  String content;
  @HiveField(2)
  String ownerTag;
  @HiveField(3)
  String type;
  @HiveField(4)
  DateTime createdAt;
  @HiveField(5)
  DateTime updatedAt;
  @HiveField(6)
  String conversationId;
  @HiveField(7)
  List<String> notDeliveredTo;

  Message({
    this.id,
    this.conversationId,
    this.type,
    this.content,
    this.ownerTag,
    this.createdAt,
    this.notDeliveredTo,
  });

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    ownerTag = json['ownerTag'];
    createdAt =
        json['createdAt'] != null ? DateTime.parse(json['createdAt']) : null;
    updatedAt =
        json['updatedAt'] != null ? DateTime.parse(json['updatedAt']) : null;
    type = json['type'];
    conversationId = json['conversationId'];

    List<dynamic> parsed = jsonDecode(json['notDeliveredTo']);
    notDeliveredTo = [];
    for (var j = 0; j < (parsed.length); j++) {
      notDeliveredTo.add(parsed[j].toString());
    }
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'content': content,
        'ownerTag': ownerTag,
        'createdAt': createdAt?.toIso8601String(),
        'type': type,
        'updatedAt': updatedAt?.toIso8601String(),
        'conversationId': conversationId,
        'notDeliveredTo': jsonEncode(notDeliveredTo)
      };

  @override
  int compareTo(other) {
    return createdAt.compareTo(other.createdAt);
  }
}
