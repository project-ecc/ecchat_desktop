//@dart=2.7
import 'package:hive/hive.dart';
part 'participant.g.dart';

@HiveType(typeId: 3)
class Participant extends HiveObject {
  @HiveField(0)
  String routingTag;
  @HiveField(1)
  String nickName;
  @HiveField(2)
  String conversationId;
  @HiveField(3)
  String colorHex;
  @HiveField(4)
  String status;
  @HiveField(5)
  String joinStatus;
  @HiveField(6)
  String lastSeen;
  @HiveField(7)
  String color;

  Participant({
    this.routingTag,
    this.nickName,
    this.conversationId,
    this.colorHex,
    this.status,
    this.joinStatus,
    this.lastSeen,
    this.color,
  });

  Participant.fromJson(Map<String, dynamic> json)
      : routingTag = json['routingTag'],
        nickName = json['nickName'],
        conversationId = json['conversationId'],
        status = json['status'],
        joinStatus = json['joinStatus'],
        lastSeen = json['lastSeen'],
        colorHex = json['colorHex'];

  Map<String, dynamic> toJson() => {
        'routingTag': routingTag,
        'nickName': nickName,
        'conversationId': conversationId,
        'status': status,
        'joinStatus': joinStatus,
        'lastSeen': lastSeen,
        'colorHex': colorHex
      };
}
