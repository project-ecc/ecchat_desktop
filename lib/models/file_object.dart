//@dart=2.7
class FileObject {
  String mediaId;
  String conversationId;
  String messageId;
  String name;
  String data;
  int size;
  String type;

  FileObject({
    this.name,
    this.data,
    this.size,
    this.type,
    this.mediaId,
    this.conversationId,
    this.messageId,
  });

  FileObject.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        mediaId = json['mediaId'],
        data = json['data'],
        size = json['size'],
        type = json['type'],
        conversationId = json['conversationId'],
        messageId = json['messageId'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'mediaId': mediaId,
        'data': data,
        'size': size,
        'type': type,
        'conversationId': conversationId,
        'messageId': messageId
      };
}
