//@dart=2.7
import 'package:ecchat_desktop/models/message.dart';
import 'package:ecchat_desktop/models/participant.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:hive/hive.dart';
part 'conversation.g.dart';

@HiveType(typeId: 1)
class Conversation extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String imageUrl;
  @HiveField(3)
  String createdAt;
  @HiveField(4)
  String creatorTag;
  @HiveField(5)
  HiveList<Message> messages;
  @HiveField(6)
  HiveList<Participant> participants;
  @HiveField(7)
  String ownerid;

  Box participantsBox = Hive.box<Participant>('participantsBox');
  Box messageBox = Hive.box<Message>('messageBox');

  Conversation(
      {@required this.id,
      @required this.name,
      @required this.imageUrl,
      @required this.createdAt,
      this.creatorTag,
      this.messages,
      this.participants,
      this.ownerid});

  Conversation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    createdAt = json['createdAt'];
    creatorTag = json['creatorTag'];
    ownerid = json['ownerId'];
    List<dynamic> participantList = jsonDecode(json['participants']);
    participants = HiveList(participantsBox);
    for (var i = 0; i < (participantList.length); i++) {
      Participant p = Participant.fromJson(participantList[i]);
      participantsBox.add(p);
      participants.add(p);
    }
    List<dynamic> messageList = jsonDecode(json['messages']);
    messages = HiveList(messageBox);
    for (var j = 0; j < (messageList.length); j++) {
      Message m = Message.fromJson((messageList[j]));
      messageBox.add(m);
      messages.add(m);
    }
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'createdAt': createdAt,
        'creatorTag': creatorTag,
        'participants': jsonEncode(participants),
        'messages': jsonEncode(messages),
        'ownerId': ownerid
      };
}
