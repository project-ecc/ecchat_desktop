//@dart=2.7
class Wallet {
  int version = 0;
  int protocolVersion = 0;
  int walletVersion = 0;
  double balance = 0.0;
  double newMint = 0.0;
  double stake = 0.0;
  int blocks = 0;
  int headers = 0;
  double moneySupply = 0.0;
  int connnections = 0;
  double difficulty = 0.0;
  bool testnet = false;
  int keyPoolOldest = 0;
  int keyPoolSize = 0;
  bool encrypted = false;
  int unlockedUntil = 0;

  void setInfo(Map<String, dynamic> payload) {
    this.version = payload['version'];
    this.protocolVersion = payload['protocolversion'];
    this.walletVersion = payload['walletversion'];
    this.balance = payload['balance'];
    this.newMint = payload['newmint'];
    this.stake = payload['stake'];
    this.blocks = payload['blocks'];
    this.headers = payload['headers'];
    this.moneySupply = payload['moneysupply'];
    this.connnections = payload['connections'];
    this.difficulty = payload['difficulty'];
    this.testnet = payload['testnet'];
    this.keyPoolOldest = payload['keypoololdest'];
    this.keyPoolSize = payload['keypoolsize'];
    this.encrypted = payload['encrypted'];
    this.unlockedUntil = payload['unlocked_until'];
  }
}
