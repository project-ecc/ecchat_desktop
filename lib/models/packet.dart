//@dart=2.7
import 'package:hive/hive.dart';
part 'packet.g.dart';

@HiveType(typeId: 4)
class Packet extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String ver;
  @HiveField(2)
  String to;
  @HiveField(3)
  String from;
  @HiveField(4)
  String meth;
  @HiveField(5)
  String data;

  Packet({this.id, this.ver, this.to, this.from, this.meth, this.data});

  Packet.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        ver = json['ver'],
        to = json['to'],
        from = json['from'],
        meth = json['meth'],
        data = json['data'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'ver': ver,
        'to': to,
        'from': from,
        'meth': meth,
        'data': data,
      };
}
