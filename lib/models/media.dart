//@dart=2.7
import 'package:flutter/material.dart';

class Media {
  String id;
  String name;
  String location;

  Media({@required this.id, @required this.name, @required this.location});

  Media.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        location = json['location'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'location': location,
      };
}
