//@dart=2.7
import 'package:ecchat_desktop/extensions.dart';
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/models/message.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:ecchat_desktop/widgets/message_detail.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_emoji/flutter_emoji.dart';

class MessageItem extends StatefulWidget {
  @override
  _MessageItemState createState() => _MessageItemState();
  final Conversation conversation;
  final Message message;
  final Message prevMessage;
  final Function editFunction;
  final Function deleteFunction;

  MessageItem({
    this.conversation,
    this.message,
    this.prevMessage,
    this.editFunction,
    this.deleteFunction,
  });
}

class _MessageItemState extends State<MessageItem> {
  var _tapPosition;
  GlobalKey _key = GlobalKey();

  void _storePosition(PointerHoverEvent details) {
    _tapPosition = details.position;
  }

  _popupMenu(BuildContext ctx, Message m) async {
    var account = Provider.of<MessagingClient>(context, listen: false).account;
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    await showMenu(
      context: ctx,
      position: RelativeRect.fromRect(
        _tapPosition & Size(40, 40), // smaller rect, the touch area
        Offset.zero & overlay.size, // Bigger rect, the entire screen
      ),
      color: Theme.of(context).canvasColor,
      items: [
        if (m.ownerTag == account.routingTag)
          PopupMenuItem(
            value: 1,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(
                    Icons.edit,
                    color: Theme.of(context).iconTheme.color,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    showDialog(
                      context: ctx,
                      builder: (context) => AlertDialog(
                        backgroundColor: Theme.of(context).primaryColor,
                        content: MessageDetail(),
                      ),
                    );
                  },
                  child: Text(
                    'Details',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                )
              ],
            ),
          ),
        if (m.ownerTag == account.routingTag)
          PopupMenuItem(
            value: 1,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(
                    Icons.edit,
                    color: Theme.of(context).iconTheme.color,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    widget.editFunction(m);
                  },
                  child: Text(
                    'Edit',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                )
              ],
            ),
          ),
        if (m.ownerTag == account.routingTag)
          PopupMenuItem(
            value: 2,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                  child: Icon(
                    Icons.delete,
                    color: Theme.of(context).iconTheme.color,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    widget.deleteFunction(m);
                  },
                  child: Text(
                    'Delete',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                )
              ],
            ),
          ),
        PopupMenuItem(
          value: 3,
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(2, 2, 8, 2),
                child: Icon(
                  Icons.arrow_back,
                  color: Theme.of(context).iconTheme.color,
                ),
              ),
              FlatButton(
                onPressed: () {},
                child: Text(
                  'Reply',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _getUserNameForMessage(Message m) {
    var p = widget.conversation.participants
        .where((element) => element.routingTag == m.ownerTag);
    var uName = '';
    if (p.first != null) {
      uName = p.first.nickName;
    } else {
      uName = m.ownerTag.substring(1, 8);
    }

    return Text(
      uName,
      style: Theme.of(context).textTheme.bodyText2.copyWith(
            fontWeight: FontWeight.bold,
            color: p.first.colorHex != null
                ? HexColor.fromHex(p.first.colorHex)
                : Theme.of(context).textTheme.bodyText1.color,
          ),
      overflow: TextOverflow.ellipsis,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3),
      child: Column(
        children: [
          if (widget.prevMessage == null ||
              (widget.prevMessage != null &&
                  widget.message.ownerTag != widget.prevMessage.ownerTag) ||
              widget.message.createdAt.day
                      .compareTo(widget.prevMessage.createdAt.day) >
                  0)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 150,
                  child: _getUserNameForMessage(widget.message),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                    timeago.format(
                      widget.message.createdAt,
                    ),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: Colors.grey)),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: GestureDetector(
                  child: MouseRegion(
                    onHover: _storePosition,
                    child: SelectableText(
                      EmojiParser().emojify(widget.message.content),
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(fontSize: 18),
                      key: _key,
                      onTap: () {
                        _popupMenu(context, widget.message);
                      },
                    ),
                  ),
                ),
              ),
              if (widget.message.updatedAt != null)
                Text(
                  '(Edited)',
                  style: Theme.of(context).textTheme.subtitle2,
                ),
            ],
          ),
          SizedBox(
            height: 5,
          )
        ],
      ),
    );
  }
}
