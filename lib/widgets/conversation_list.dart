//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:flutter/material.dart';
import 'package:ecchat_desktop/widgets/conversation_item.dart';
import '../services/messaging_client.dart';
import 'package:provider/provider.dart';
import 'package:beamer/beamer.dart';

class ConversationList extends StatefulWidget {
  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: 500,
      child: Consumer<MessagingClient>(
        builder: (ctx, messagingClient, _) =>
            messagingClient.conversationBox.values.length > 0
                ? ListView.builder(
                    itemCount: messagingClient.conversationBox.values.length,
                    itemBuilder: (ctx, i) {
                      Conversation c = messagingClient.conversationBox.getAt(i);
                      return Column(
                        children: [
                          ConversationItem(
                            c.id,
                            c.name,
                            c.imageUrl,
                            Beamer.of(context)
                                .currentLocation
                                .state
                                .uri
                                .pathSegments
                                .contains(c.id),
                          ),
                          Divider(
                            color: Color.fromARGB(50, 206, 206, 206),
                            endIndent: 10,
                          )
                        ],
                      );
                    },
                  )
                : Text('Please start a conversation'),
      ),
    );
  }
}
