//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/widgets/participants_item.dart';
import 'package:flutter/material.dart';

class ParticipantsList extends StatelessWidget {
  final Conversation conversation;
  ParticipantsList({this.conversation});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 400,
      height: 450,
      child: ListView.builder(
        itemCount: conversation.participants.length,
        itemBuilder: (ctx, i) => Column(
          children: [
            ParticipantsItem(conversation, conversation.participants[i]),
            Divider(
              color: Color.fromARGB(50, 206, 206, 206),
              endIndent: 10,
            )
          ],
        ),
      ),
    );
  }
}
