//@dart=2.7
import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:swipe_to/swipe_to.dart';

class ConversationItem extends StatefulWidget {
  final String id;
  final String username;
  final String imageUrl;
  final bool isActive;

  ConversationItem(this.id, this.username, this.imageUrl, this.isActive);

  @override
  _ConversationItemState createState() => _ConversationItemState();
}

class _ConversationItemState extends State<ConversationItem> {
  @override
  Widget build(BuildContext context) {
    return SwipeTo(
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(
          vertical: 10,
        ),
        onTap: () => context.beamToNamed(
          '/messaging/${widget.id}',
        ),
        leading: Container(
          child: CircleAvatar(
            radius: 30,
            child: Text(
              widget.username.substring(0, 1).toUpperCase(),
              style: Theme.of(context).textTheme.headline1,
            ),
            backgroundColor: Colors.grey,
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            widget.isActive
                ? Text(
                    widget.username,
                    style: Theme.of(context).textTheme.headline6.copyWith(
                          color: Theme.of(context).accentColor,
                        ),
                    overflow: TextOverflow.ellipsis,
                  )
                : Text(
                    widget.username,
                    style: Theme.of(context).textTheme.headline6,
                    overflow: TextOverflow.ellipsis,
                  ),
          ],
        ),
        trailing: IconButton(
          icon: Icon(Icons.more_vert),
          onPressed: () {},
          color: Theme.of(context).iconTheme.color,
        ),
      ),
    );
  }
}
