//@dart=2.7
import 'package:flutter/material.dart';

class NavButton extends StatefulWidget {
  final double width;
  final double height;
  final Icon icon;
  final String text;
  final Function onClick;
  final Color color;

  NavButton({
    this.icon,
    this.text,
    this.color,
    this.height,
    this.width,
    this.onClick,
  });

  @override
  _NavButtonState createState() => _NavButtonState();
}

class _NavButtonState extends State<NavButton> {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: new BoxConstraints(
        minHeight: widget.height ?? 100.0,
        minWidth: widget.width ?? 150.0,
      ),
      child: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconButton(
              icon: widget.icon,
              onPressed: widget.onClick,
              iconSize: 50,
              color: Theme.of(context)
                  .iconTheme
                  .copyWith(
                    color: widget.color,
                  )
                  .color,
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              widget.text,
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: widget.color,
                  ),
            )
          ],
        ),
      ),
    );
  }
}
