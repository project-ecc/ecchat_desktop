import 'package:flutter/material.dart';

class MessageDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Text(
                'Message Details',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Dismiss'),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}
