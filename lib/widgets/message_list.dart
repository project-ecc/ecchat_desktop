//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/widgets/message_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

class MessageList extends StatefulWidget {
  final Conversation _conversation;
  final Function editFunction;
  final Function deleteFunction;

  MessageList(this._conversation, this.editFunction, this.deleteFunction);

  @override
  _MessageListState createState() => _MessageListState();
}

class _MessageListState extends State<MessageList> {
  final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _controller.jumpTo(_controller.position.maxScrollExtent);
    });
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GroupedListView<dynamic, String>(
        elements: widget._conversation.messages,
        groupBy: (element) => element.createdAt.day.toString(),
        groupHeaderBuilder: (element) => Container(
          height: 40,
          child: Align(
            child: Container(
              width: 120,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  '${DateFormat.yMMMd().format(element.createdAt)}',
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(color: Colors.white54),
                ),
              ),
            ),
          ),
        ),
        controller: _controller,
        indexedItemBuilder: (context, dynamic element, int i) => MessageItem(
          conversation: widget._conversation,
          message: element,
          prevMessage: i != 0 ? widget._conversation.messages[i - 1] : null,
          editFunction: widget.editFunction,
          deleteFunction: widget.deleteFunction,
        ),
        // itemComparator: (item1, item2) => item1['name'].compareTo(item2['name']), // optional
        // useStickyGroupSeparators: true, // optional
        floatingHeader: true, // optional
        order: GroupedListOrder.ASC, // optional
      ),
    );
  }
}
