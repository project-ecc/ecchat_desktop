//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/models/participant.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ParticipantInfo extends StatefulWidget {
  @override
  _ParticipantInfoState createState() => _ParticipantInfoState();
  final Participant participant;
  final Conversation conversation;
  ParticipantInfo(this.conversation, this.participant);
}

class _ParticipantInfoState extends State<ParticipantInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.8,
      height: MediaQuery.of(context).size.height / 2.0,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Container(
                width: 500,
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    widget.participant.nickName,
                    style: Theme.of(context).textTheme.bodyText1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Dismiss'),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  'Nickname',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  widget.participant.nickName,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  'Routing Tag',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Expanded(
                flex: 2,
                child: SelectableText(
                  widget.participant.routingTag,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  'Join Status',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  widget.participant.joinStatus,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  'Status',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  widget.participant.status,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  'Last Seen',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  widget.participant.lastSeen ?? 'Never',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            ],
          ),
          SizedBox(
            height: 40,
          ),
          Text(
            'Actions',
            style: Theme.of(context).textTheme.headline6,
          ),
          Row(
            children: [
              RaisedButton.icon(
                color: Theme.of(context).accentColor,
                icon: Icon(Icons.send),
                label: Text('Resend'),
                onPressed: () {
                  var messagingClient =
                      Provider.of<MessagingClient>(context, listen: false);
                  messagingClient.reInviteParticipant(
                    widget.conversation,
                    widget.participant,
                  );
                },
              )
            ],
          )
        ],
      ),
    );
  }
}
