//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/models/participant.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:ecchat_desktop/widgets/participant_info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ParticipantsItem extends StatelessWidget {
  final Participant participant;
  final Conversation conversation;

  ParticipantsItem(this.conversation, this.participant);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(
          vertical: 10,
        ),
        onTap: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              backgroundColor: Theme.of(context).primaryColor,
              content: ParticipantInfo(conversation, participant),
            ),
          );
        },
        leading: Container(
          child: CircleAvatar(
            radius: 30,
            child: Text(
              participant.nickName.substring(0, 1).toUpperCase(),
              style: Theme.of(context).textTheme.headline1,
            ),
            backgroundColor: Colors.grey,
          ),
        ),
        title: SizedBox(
          width: 50,
          child: Text(
            participant.nickName,
            style: Theme.of(context).textTheme.bodyText1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        subtitle: Text(
          participant.joinStatus,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        trailing: Container(
          child: Consumer<MessagingClient>(
            builder: (ctx, messagingClient, _) => CircleAvatar(
              backgroundColor: participant.status == 'online' ||
                      participant.routingTag ==
                          messagingClient.account.routingTag
                  ? Colors.green
                  : Colors.red,
              radius: 10,
            ),
          ),
        ),
      ),
    );
  }
}
