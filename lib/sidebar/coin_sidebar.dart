//@dart=2.7
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';
import 'package:provider/provider.dart';

class CoinSideBar extends StatefulWidget {
  @override
  _CoinSideBarState createState() => _CoinSideBarState();
}

class _CoinSideBarState extends State<CoinSideBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 50,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white,
              width: 2,
            ),
          ),
          child: Row(
            children: [
              Text(
                'Balance',
                style: Theme.of(context).textTheme.bodyText2,
              ),
              Consumer<MessagingClient>(
                builder: (context, value, child) => Text(
                  value.clientList['ECC'].walletState.balance.toString(),
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              )
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height - 80,
          child: ListView(
            padding: EdgeInsets.all(8),
            children: [
              ListTile(
                contentPadding: EdgeInsets.symmetric(
                  vertical: 10,
                ),
                leading: Icon(
                  Icons.money,
                  size: 30,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text(
                  'Overview',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                        color: Beamer.of(context)
                                .currentLocation
                                .state
                                .uri
                                .pathSegments
                                .contains('overview')
                            ? Theme.of(context).accentColor
                            : Theme.of(context).textTheme.bodyText1.color,
                      ),
                ),
                onTap: () => context.beamToNamed(
                  '/wallet/overview',
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(
                  vertical: 10,
                ),
                leading: Icon(
                  Icons.arrow_forward,
                  size: 30,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text('Send',
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          color: Beamer.of(context)
                                  .currentLocation
                                  .state
                                  .uri
                                  .pathSegments
                                  .contains('send')
                              ? Theme.of(context).accentColor
                              : Theme.of(context).textTheme.bodyText1.color,
                        )),
                onTap: () => context.beamToNamed(
                  '/wallet/send',
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(
                  vertical: 10,
                ),
                leading: Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text(
                  'Receive',
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: Beamer.of(context)
                                .currentLocation
                                .state
                                .uri
                                .pathSegments
                                .contains('receive')
                            ? Theme.of(context).accentColor
                            : Theme.of(context).textTheme.bodyText1.color,
                      ),
                ),
                onTap: () => context.beamToNamed(
                  '/wallet/receive',
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(
                  vertical: 10,
                ),
                leading: Icon(
                  Icons.all_inbox_rounded,
                  size: 30,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text(
                  'Transactions',
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: Beamer.of(context)
                                .currentLocation
                                .state
                                .uri
                                .pathSegments
                                .contains('transactions')
                            ? Theme.of(context).accentColor
                            : Theme.of(context).textTheme.bodyText1.color,
                      ),
                ),
                onTap: () => context.beamToNamed(
                  '/wallet/transactions',
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
