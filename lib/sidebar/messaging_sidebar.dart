//@dart=2.7
import 'package:ecchat_desktop/screens/messaging/new_conversation.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../widgets/conversation_list.dart';

class MessagingSidebar extends StatefulWidget {
  @override
  _MessagingSidebarState createState() => _MessagingSidebarState();
}

class _MessagingSidebarState extends State<MessagingSidebar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Consumer<MessagingClient>(
                builder: (ctx, messagingClient, _) => Text(
                  messagingClient.account.nickname,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            ),
          ],
        ),
        Container(
          height: MediaQuery.of(context).size.height - 80,
          child: ConversationList(),
        ),
        Row(
          children: [
            Expanded(
              child: RaisedButton(
                color: Theme.of(context).accentColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('New Conversation'),
                    Icon(Icons.add),
                  ],
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      backgroundColor: Theme.of(context).primaryColor,
                      content: NewConversation(),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ],
    );
  }
}
