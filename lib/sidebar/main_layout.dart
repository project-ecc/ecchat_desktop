//@dart=2.7
import 'package:beamer/beamer.dart';
import 'package:ecchat_desktop/locations/message_location.dart';
import 'package:ecchat_desktop/locations/wallet_locations.dart';
import 'package:ecchat_desktop/widgets/nav_button.dart';
import 'package:flutter/material.dart';

class MainLayout extends StatefulWidget {
  final _beamerKey = GlobalKey<BeamerState>();
  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  @override
  Widget build(BuildContext context) {
    var routerDelegate = BeamerRouterDelegate(
      locationBuilder: BeamerLocationBuilder(
        beamLocations: [
          WalletRootLocation(),
          MessageLocation(),
        ],
      ),
    );
    return Scaffold(
      // left side
      body: Row(
        children: [
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height - 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      NavButton(
                        icon: Icon(
                          Icons.person,
                          size: 45,
                        ),
                        text: 'Profile',
                        color: Beamer.of(context)
                                .currentConfiguration
                                .pathSegments
                                .contains('profile')
                            ? Theme.of(context).accentColor
                            : Colors.white,
                        onClick: () => widget
                            ._beamerKey.currentState.routerDelegate
                            .beamToNamed(
                          '/profile',
                        ),
                      ),
                      NavButton(
                        icon: Icon(
                          Icons.account_balance_wallet,
                          size: 45,
                        ),
                        text: 'Wallet',
                        color: Beamer.of(context)
                                .currentConfiguration
                                .pathSegments
                                .contains('wallet')
                            ? Theme.of(context).accentColor
                            : Colors.white,
                        onClick: () => widget
                            ._beamerKey.currentState.routerDelegate
                            .beamToNamed(
                          '/wallet',
                        ),
                      ),
                      NavButton(
                        icon: Icon(
                          Icons.mail,
                          size: 45,
                        ),
                        text: 'Messaging',
                        color: Beamer.of(context)
                                .currentConfiguration
                                .pathSegments
                                .contains('messaging')
                            ? Theme.of(context).accentColor
                            : Colors.white,
                        onClick: () => widget
                            ._beamerKey.currentState.routerDelegate
                            .beamToNamed(
                          '/messaging',
                        ),
                      ),
                      // NavButton(
                      //   Icon(
                      //     Icons.monetization_on,
                      //     size: 45,
                      //   ),
                      //   'Wallet',
                      //   100.0,
                      //   100.0,
                      //   () => _beamerKey.currentState.routerDelegate.beamToNamed(
                      //     '/wallet',
                      //   ),
                      // ),
                      // NavButton(
                      //   Icon(
                      //     Icons.block,
                      //     size: 45,
                      //   ),
                      //   'Chains',
                      //   Beamer.of(context)
                      //           .currentConfiguration
                      //           .pathSegments
                      //           .contains('chains')
                      //       ? Theme.of(context).accentColor
                      //       : Colors.white,
                      //   100.0,
                      //   100.0,
                      //   () =>
                      //       widget._beamerKey.currentState.routerDelegate.beamToNamed(
                      //     '/chains',
                      //   ),
                      // ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Image.asset(
                      'assets/images/ecc_logo.png',
                      width: 150,
                    ),
                    Text(
                      'v 1.0.3',
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ],
                )
              ],
            ),
          ),
          Expanded(
            flex: 11,
            child: Beamer(
              key: widget._beamerKey,
              routerDelegate: routerDelegate,
            ),
          ),
        ],
      ),
      // main content window
    );
  }
}
