//@dart=2.7
import 'dart:async';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:ffi/ffi.dart';
import 'zmq_bindings.dart';

ZmqBindings _bindings;

void _initializeBindings() {
  if (_bindings == null) {
    // todo: cross-platform?
    DynamicLibrary _lib = DynamicLibrary.open('libzmq.5.dylib');
    _bindings = ZmqBindings(_lib);
  }
}

/// High-level wrapper around the ØMQ C api.
class ZeroMQ {
  Pointer<Void> _context;
  Pointer<Void> _poller;

  bool _isActive = true;
  bool _pollingMicrotaskScheduled = false;

  final Map<Pointer<Void>, ZmqSocket> _createdSockets = {};
  final List<ZmqSocket> _listening = [];
  Completer _stopCompleter;

  ZeroMQ() {
    _initializeBindings();
    _context = _bindings.zmq_ctx_new();
    _poller = _bindings.zmq_poller_new();
    _startPolling();
  }

  Future stop() {
    _isActive = false;
    _stopCompleter = Completer();
    return _stopCompleter.future;
  }

  Pointer<zmq_msg_t> _allocateMessage() {
    return calloc.allocate<Uint8>(64).cast();
  }

  void _startPolling() {
    if (!_pollingMicrotaskScheduled && _listening.isNotEmpty) {
      _pollingMicrotaskScheduled = true;
      scheduleMicrotask(_poll);
    }
  }

  void _poll() {
    final listeners = _listening.length;
    final events = calloc.allocate<zmq_poller_event_t>(listeners);
    final readEvents =
        _bindings.zmq_poller_wait_all(_poller, events, listeners, 0);

    final msg = _allocateMessage();
    for (var i = 0; i < readEvents; i++) {
      final event = events[i];
      final socket = _createdSockets[event.socket];

      final message = ZmqMessage();

      // Receive multiple message parts
      while (true) {
        var rc = _bindings.zmq_msg_init(msg);
        _checkSuccess(rc);

        rc = _bindings.zmq_msg_recv(msg, socket._handle, 0);
        _checkSuccess(rc, positiveIsSuccess: true);

        final size = _bindings.zmq_msg_size(msg);
        final data = _bindings.zmq_msg_data(msg).cast<Uint8>();

        final copyOfData = Uint8List.fromList(data.asTypedList(size));
        final hasNext = _bindings.zmq_msg_more(msg) != 0;

        if (hasNext) {
          message.topic = copyOfData;
        } else {
          message.payload = copyOfData;
        }
        if (!hasNext) break;
      }
      socket._controller.add(message);
      _bindings.zmq_msg_close(msg);
    }

    calloc.free(msg);
    calloc.free(events);

    // After the polling iteration, re-schedule another one if necessary.
    if (_isActive) {
      if (_listening.isNotEmpty) {
        // NOT using scheduleMicrotask because it blocks up the queue
        Timer.run(_poll);
        return;
      }
    } else {
      _shutdownInternal();
      _stopCompleter.complete(null);
    }
    // no polling necessary, reset flag so that the next call to _startPolling
    // will bring the mechanism back up.
    _pollingMicrotaskScheduled = false;
  }

  ZmqSocket createSocket(SocketMode mode) {
    final socket = _bindings.zmq_socket(_context, mode.index);
    final apiSocket = ZmqSocket(socket, this);
    _createdSockets[socket] = apiSocket;
    return apiSocket;
  }

  void _listen(ZmqSocket socket) {
    _bindings.zmq_poller_add(_poller, socket._handle, nullptr, ZMQ_POLLIN);
    _listening.add(socket);
    _startPolling();
  }

  void _stopListening(ZmqSocket socket) {
    _bindings.zmq_poller_remove(_poller, socket._handle);
    _listening.remove(socket);
  }

  void _handleSocketClosed(ZmqSocket socket) {
    if (_isActive) {
      _createdSockets.remove(socket._handle);
    }
    if (_listening.contains(socket)) {
      _stopListening(socket);
    }
  }

  void _shutdownInternal() {
    for (final socket in _createdSockets.values) {
      socket.close();
    }
    _createdSockets.clear();
    _listening.clear();

    _bindings.zmq_ctx_term(_context);

    final pollerPtrPtr = calloc<Pointer<Void>>();
    pollerPtrPtr.value = _poller;
    _bindings.zmq_poller_destroy(pollerPtrPtr);
    calloc.free(pollerPtrPtr);
  }

  void _checkSuccess(int statusCode, {bool positiveIsSuccess = false}) {
    final isFailure = positiveIsSuccess ? statusCode < 0 : statusCode != 0;

    if (isFailure) {
      final errorCode = _bindings.zmq_errno();
      throw ZeroMQException(errorCode);
    }
  }
}

enum SocketMode {
  pair,
  pub,
  sub,
  req,
  rep,
  dealer,
  router,
  pull,
  push,
  xPub,
  xSub,
  stream
}

class ZmqMessage {
  Uint8List _topic;
  Uint8List _payload = Uint8List(0);

  set topic(Uint8List topic) {
    this._topic = topic;
  }

  get topic => String.fromCharCodes(_topic);

  get payload => _payload;

  set payload(Uint8List nextLine) {
    BytesBuilder b = BytesBuilder();
    b.add(_payload);
    b.add(nextLine);
    this._payload = b.toBytes();
  }
}

class ZmqSocket {
  final Pointer<Void> _handle;
  final ZeroMQ _zmq;

  bool _closed = false;

  StreamController<ZmqMessage> _controller;
  Stream<ZmqMessage> get messages => _controller.stream;

  ZmqSocket(this._handle, this._zmq) {
    _controller = StreamController(onListen: () {
      _zmq._listen(this);
    }, onCancel: () {
      _zmq._stopListening(this);
    });
  }

  /// Sends the [data] payload over this socket.
  ///
  /// The [more] parameter (defaults to false) signals that this is a multi-part
  /// message. ØMQ ensures atomic delivery of messages: peers shall receive
  /// either all message parts of a message or none at all.
  void send(List<int> data, {bool more = false}) {
    _checkNotClosed();
    final ptr = calloc.allocate<Uint8>(data.length);
    ptr.asTypedList(data.length).setAll(0, data);

    final sendParams = more ? ZMQ_SNDMORE : 0;
    final result =
        _bindings.zmq_send(_handle, ptr.cast(), data.length, sendParams);
    _zmq._checkSuccess(result, positiveIsSuccess: true);
    calloc.free(ptr);
  }

  void bind(String address) {
    _checkNotClosed();
    final endpointPointer = address.toNativeUtf8();
    final result = _bindings.zmq_bind(_handle, endpointPointer.cast());
    _zmq._checkSuccess(result);
    calloc.free(endpointPointer);
  }

  void connect(String address) {
    _checkNotClosed();
    final endpointPointer = address.toNativeUtf8();
    final result = _bindings.zmq_connect(_handle, endpointPointer.cast());
    _zmq._checkSuccess(result);
    calloc.free(endpointPointer);
  }

  void filterTopic(String topic) {
    _checkNotClosed();
    final topicPointer = topic.toNativeUtf8();
    print(topic.length);
    print(topicPointer.length);
    final result = _bindings.zmq_setsockopt(
        _handle, ZMQ_SUBSCRIBE, topicPointer.cast(), topic.length);
    _zmq._checkSuccess(result);
    // calloc.free(topicPointer);
  }

  void close() {
    if (!_closed) {
      _zmq._handleSocketClosed(this);
      _bindings.zmq_close(_handle);
      _controller.close();
      _closed = true;
    }
  }

  void _checkNotClosed() {
    if (_closed) {
      throw StateError("This operation can't be performed on a cosed socket!");
    }
  }
}

class ZeroMQException implements Exception {
  final int errorCode;

  ZeroMQException(this.errorCode);

  @override
  String toString() {
    final msg = _errorMessages[errorCode];
    if (msg == null) {
      return 'ZeroMQException($errorCode)';
    } else {
      return 'ZeroMQException: $msg';
    }
  }

  static const Map<int, String> _errorMessages = {
    EPROTONOSUPPORT: 'The requested transport protocol is not supported',
    EADDRINUSE: 'The requested address is already in use',
  };
}
