//@dart=2.7
import 'package:beamer/beamer.dart';
import 'package:ecchat_desktop/locations/conversation_location.dart';
import 'package:ecchat_desktop/sidebar/messaging_sidebar.dart';
import 'package:flutter/material.dart';

class MessagingLayout extends StatefulWidget {
  final _beamerKey = GlobalKey<BeamerState>();
  @override
  _MessagingLayoutState createState() => _MessagingLayoutState();
}

class _MessagingLayoutState extends State<MessagingLayout> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Container(
            color: Colors.black87,
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: MessagingSidebar(),
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: Beamer(
            key: widget._beamerKey,
            routerDelegate: BeamerRouterDelegate(
              locationBuilder: BeamerLocationBuilder(
                beamLocations: [
                  ConversationLocation(),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
