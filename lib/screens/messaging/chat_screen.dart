//@dart=2.7
import 'package:ecchat_desktop/screens/messaging/conversation_info.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'message_screen.dart';

class ChatScreen extends StatefulWidget {
  final String conversationId;
  ChatScreen({this.conversationId});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  Animation _animation;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    final Animation curve = CurvedAnimation(
        parent: _animationController, curve: Curves.easeInOutBack);
    _animation = IntTween(begin: 0, end: 40).animate(curve);
    // _animation.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    var conversation = Provider.of<MessagingClient>(context)
        .findConversationById(this.widget.conversationId);

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(40.0),
        child: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          backgroundColor: Color(0xff292c2d),
          title: Text(
            conversation.name.toUpperCase(),
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                if (_animationController.value == 0.0) {
                  _animationController.forward();
                } else {
                  _animationController.reverse();
                }
              },
            ),
          ],
        ),
      ),
      body: Row(
        children: [
          Expanded(
            flex: 100,
            child: MessageScreen(conversation: conversation),
          ),
          AnimatedBuilder(
            animation: _animationController,
            builder: (_, child) {
              return Expanded(
                flex: _animation.value,
                // Uses to hide widget when flex is going to 0
                child: Container(
                  width: 0,
                  child: ConversationInfo(conversation: conversation),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
