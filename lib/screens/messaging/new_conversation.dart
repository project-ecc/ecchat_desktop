//@dart=2.7
import 'dart:async';

import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../services/daemon_client.dart';

class NewConversation extends StatefulWidget {
  static const routeName = '/message-new';
  @override
  _NewConversationState createState() => _NewConversationState();
}

class _NewConversationState extends State<NewConversation> {
  TextEditingController addressInput = TextEditingController();
  FocusNode addressFocus = FocusNode();

  TextEditingController conversationInput = TextEditingController();
  FocusNode conversationFocusNode = FocusNode();

  bool haveRoute = false;
  String routingTag;
  String conversationName;
  bool shareNick = false;
  bool doneSearch = false;

  void _newConversation() async {
    var tagInput = addressInput.text;
    var daemonConnection =
        await Provider.of<DaemonClient>(context, listen: false)
            .haveRoute(tagInput);
    setState(() {
      haveRoute = daemonConnection;
      routingTag = tagInput;
      conversationName = conversationInput.text;
    });
  }

  Future<void> _findRoute() async {
    await Provider.of<DaemonClient>(context).findRoute(routingTag);
    Timer(Duration(seconds: 10), () async {
      Navigator.of(context, rootNavigator: true).pop('dialog');
      print("Yeah, this line is printed after 10 seconds");
      var haveRoute = await Provider.of<DaemonClient>(context, listen: false)
          .haveRoute(routingTag);
      setState(() {
        haveRoute = haveRoute;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var conversationProvider =
        Provider.of<MessagingClient>(context, listen: false);

    return Container(
      width: MediaQuery.of(context).size.width / 1.8,
      height: MediaQuery.of(context).size.height / 2.0,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Text(
                'Start new conversation',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Talk Now!'),
                disabledColor: Theme.of(context).accentColor,
                onPressed: haveRoute
                    ? () async {
                        setState(() {
                          conversationName = conversationInput.text;
                        });
                        //start conversation
                        await conversationProvider.startConversation(
                          this.routingTag,
                          this.conversationName,
                          this.shareNick,
                        );
                        Navigator.pop(context);
                      }
                    : null,
              )
            ],
          ),
          Row(
            children: [
              Expanded(
                child: TextField(
                  controller: addressInput,
                  focusNode: addressFocus,
                  onSubmitted: (value) {
                    _newConversation();
                  },
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                    hintText: 'Enter a Tag',
                  ),
                  textCapitalization: TextCapitalization.sentences,
                ),
              ),
              RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Connect'),
                onPressed: () {
                  _newConversation();
                  setState(() {
                    doneSearch = true;
                  });
                },
              )
            ],
          ),
          SizedBox(
            height: 40,
          ),
          haveRoute && doneSearch
              ? Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: conversationInput,
                            focusNode: conversationFocusNode,
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                              hintText:
                                  'Enter Conversation Name (or leave black to use routing tag)',
                            ),
                            textCapitalization: TextCapitalization.sentences,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Share nickname?',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        Checkbox(
                          activeColor: Theme.of(context).accentColor,
                          value: shareNick,
                          onChanged: (value) {
                            setState(() {
                              shareNick = !shareNick;
                            });
                          },
                        ),
                      ],
                    ),
                    Text(
                      'Congrats you have a connection, start talking now',
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                )
              : (doneSearch)
                  ? Column(
                      children: [
                        Text(
                          'You do not have a route to this tag',
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        RaisedButton.icon(
                          color: Theme.of(context).accentColor,
                          icon: Icon(Icons.search),
                          label: Text('Search'),
                          onPressed: () {
                            showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return Dialog(
                                  backgroundColor:
                                      Theme.of(context).canvasColor,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        CircularProgressIndicator(),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          "Loading",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6,
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                            _findRoute();
                          },
                        )
                      ],
                    )
                  : Container()
        ],
      ),
    );
  }
}
