//@dart=2.7
import 'package:clipboard/clipboard.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';

class AccountSettings extends StatefulWidget {
  @override
  _AccountSettingsState createState() => _AccountSettingsState();
}

class _AccountSettingsState extends State<AccountSettings> {
  TextEditingController _nicknameController = TextEditingController();
  FocusNode _nicknameFocusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  _saveAccount(String routingKey, BuildContext ctx) async {
    _formKey.currentState.save();

    if (_nicknameController.text.isEmpty) {
      await Provider.of<MessagingClient>(context, listen: false)
          .account
          .saveUserAccount(routingKey, routingKey);
    } else {
      await Provider.of<MessagingClient>(context, listen: false)
          .account
          .saveUserAccount(_nicknameController.text, routingKey);
    }
    Navigator.pop(ctx);
  }

  _loadData() async {
    var account = Provider.of<MessagingClient>(context, listen: false).account;
    _nicknameController.text = account.nickname;
  }

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.8,
      height: MediaQuery.of(context).size.height / 2.0,
      child: Consumer<MessagingClient>(
        builder: (ctx, messagingClient, _) => Form(
          key: _formKey,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Text(
                    'Account Settings',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  RaisedButton(
                    color: Theme.of(context).accentColor,
                    child: Text('Save'),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.

                        _saveAccount(messagingClient.account.routingTag, ctx);
                      }
                    },
                  )
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: _nicknameController,
                      focusNode: _nicknameFocusNode,
                      style: Theme.of(context).textTheme.bodyText1,
                      decoration: InputDecoration(
                        hintText: 'Nickname',
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 600,
                    child: SelectableText(
                      messagingClient.account.routingTag,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  RaisedButton(
                    color: Theme.of(context).accentColor,
                    onPressed: () {
                      FlutterClipboard.copy(messagingClient.account.routingTag)
                          .then((value) {
                        showSimpleNotification(
                          Text(
                            "Copied To Clipboard",
                          ),
                          position: NotificationPosition.bottom,
                          background: Colors.green,
                        );
                      });
                    },
                    child: Text('Copy to clipboard'),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
