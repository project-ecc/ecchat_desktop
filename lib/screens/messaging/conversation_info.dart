//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:ecchat_desktop/widgets/participants_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConversationInfo extends StatelessWidget {
  final Conversation conversation;
  ConversationInfo({this.conversation});

  Future<void> _leaveConversation(MessagingClient messagingClient) async {
    await messagingClient.leaveConversation(conversation);
  }

  Future<void> _deleteConversation(MessagingClient messagingClient) async {
    await messagingClient.deleteConverstaion(conversation);
  }

  Future<void> _renameConversation(MessagingClient messagingClient) async {}

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.8,
      height: MediaQuery.of(context).size.height,
      child: Consumer<MessagingClient>(
        builder: (ctx, messagingClient, _) => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Conversation Info',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '${conversation.participants.length} Participants',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(fontSize: 18),
                  ),
                  IconButton(icon: Icon(Icons.add), onPressed: () {}),
                ],
              ),
            ),
            ParticipantsList(
              conversation: conversation,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Options',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                onPressed: () {
                  _renameConversation(messagingClient);
                },
                child: Text(
                  'Rename Conversation',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                onPressed: () {},
                child: Text(
                  'Clear Contents',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FlatButton(
                onPressed: () {},
                child: Text(
                  'Block',
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        color: Colors.red,
                      ),
                ),
              ),
            ),
            messagingClient.hasJoinedConversation(conversation)
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FlatButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            actions: [
                              FlatButton.icon(
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop('dialog');
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Theme.of(context).iconTheme.color,
                                ),
                                label: Text(
                                  'No',
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                              ),
                              RaisedButton.icon(
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  _leaveConversation(messagingClient);
                                  Navigator.of(context, rootNavigator: true)
                                      .pop('dialog');
                                },
                                icon: Icon(
                                  Icons.done,
                                ),
                                label: Text(
                                  'Yes',
                                ),
                              ),
                            ],
                            backgroundColor: Theme.of(context).primaryColor,
                            content: Text(
                              'Are you sure you want to leave this converation?',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        );
                      },
                      child: Text(
                        'Leave Conversation',
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                              color: Colors.red,
                            ),
                      ),
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FlatButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            actions: [
                              FlatButton.icon(
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop('dialog');
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Theme.of(context).iconTheme.color,
                                ),
                                label: Text(
                                  'No',
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                              ),
                              RaisedButton.icon(
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  _deleteConversation(messagingClient);
                                  Navigator.of(context, rootNavigator: true)
                                      .pop('dialog');
                                },
                                icon: Icon(
                                  Icons.done,
                                ),
                                label: Text(
                                  'Yes',
                                ),
                              ),
                            ],
                            backgroundColor: Theme.of(context).primaryColor,
                            content: Text(
                              'Are you sure you want to delete this conversation?',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        );
                      },
                      child: Text(
                        'Delete Conversation',
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                              color: Colors.red,
                            ),
                      ),
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
