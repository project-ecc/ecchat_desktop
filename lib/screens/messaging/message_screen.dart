//@dart=2.7
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:ecchat_desktop/models/message.dart';
import 'package:ecchat_desktop/widgets/message_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class MessageScreen extends StatefulWidget {
  final Conversation conversation;
  MessageScreen({this.conversation});

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  TextEditingController inputField = TextEditingController();
  FocusNode inputFocus = FocusNode();
  final _formKey = GlobalKey<FormState>();
  Message editingMesage;

  GlobalKey floatingKey = LabeledGlobalKey("Floating");
  bool isFloatingOpen = false;
  OverlayEntry floating;

  Future<void> _sendMessage(MessagingClient messagingClient) async {
    if (inputField.text.isEmpty) {
      return;
    }
    _formKey.currentState.save();
    var account = Provider.of<MessagingClient>(context, listen: false).account;
    if (editingMesage == null) {
      var message = Message(
        id: Uuid().v4(),
        conversationId: widget.conversation.id,
        content: inputField.text,
        type: 'text',
        ownerTag: account.routingTag,
        createdAt: DateTime.now().toUtc(),
      );
      messagingClient.addMessageToConversation(
        widget.conversation,
        message,
      );
      messagingClient.sendMessage(
        widget.conversation,
        message,
        'messadd',
      );
    } else {
      editingMesage.content = inputField.text;
      editingMesage.updatedAt = DateTime.now().toUtc();
      editingMesage.save();

      messagingClient.editMessageInConversation(
        widget.conversation,
        editingMesage,
      );
      messagingClient.sendMessage(
        widget.conversation,
        editingMesage,
        'messedit',
      );
    }

    inputField.clear();
    inputFocus.requestFocus();
    editingMesage = null;
  }

  _joinConversation(MessagingClient messagingClient) {
    var account = Provider.of<MessagingClient>(context, listen: false).account;
    var foundParticipant = widget.conversation.participants
        .where((element) => element.routingTag == account.routingTag);
    if (foundParticipant.first != null) {
      var participant = foundParticipant.first;
      participant.joinStatus = 'joined';
      participant.nickName = account.nickname;
      participant.status = 'online';
      participant.save();
      widget.conversation.save();
      messagingClient.sendParticipant(
        widget.conversation,
        participant,
      );
    }
  }

  _joinConversationArea(MessagingClient messagingClient) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      height: 80,
      color: Color(0xff292c2d),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('You must join this conversation to send messages'),
          RaisedButton(
            child: Text('Join Now'),
            color: Theme.of(context).accentColor,
            onPressed: () {
              _joinConversation(messagingClient);
            },
          )
        ],
      ),
    );
  }

  void editFunction(Message m) {
    inputField.text = m.content;
    setState(() {
      editingMesage = m;
    });
    inputFocus.requestFocus();
  }

  void deleteFunction(Message m) {
    var messagingClient = Provider.of<MessagingClient>(context, listen: false);
    messagingClient.deleteMessageInConversation(
      m,
    );
  }

  _sendMessageArea(MessagingClient messagingClient) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      height: 80,
      color: Color(0xff292c2d),
      child: Form(
        key: _formKey,
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.photo),
              iconSize: 25,
              color: Theme.of(context).iconTheme.color,
              onPressed: () async {
                // await getImage(conversations);
              },
            ),
            Expanded(
              child: RawKeyboardListener(
                focusNode: inputFocus,
                onKey: (event) async {
                  if (event.isKeyPressed(LogicalKeyboardKey.arrowUp)) {
                    print('edit Message');
                    var message = widget.conversation.messages.last;
                    inputField.text = message.content;
                    setState(() {
                      editingMesage = message;
                    });
                    // Do something
                  }
                },
                child: TypeAheadField(
                  hideOnLoading: true,
                  getImmediateSuggestions: true,
                  direction: AxisDirection.up,
                  textFieldConfiguration: TextFieldConfiguration(
                    autofocus: true,
                    controller: inputField,
                    onSubmitted: (value) {
                      _sendMessage(messagingClient);
                    },
                    style: Theme.of(context).textTheme.bodyText1,
                    textCapitalization: TextCapitalization.sentences,
                    decoration: InputDecoration(
                      hintText: 'Send a message..',
                    ),
                  ),
                  suggestionsCallback: (pattern) async {
                    if (pattern.startsWith('/')) {
                      return {
                        {
                          'command': '/send',
                          'helpText': 'send {participant} {coin} {amount}'
                        },
                      };
                    }
                    return null;
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      leading: Icon(
                        Icons.send,
                        color: Theme.of(context).iconTheme.color,
                      ),
                      title: Text(
                        suggestion['command'],
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      subtitle: Text(
                        suggestion['helpText'],
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    );
                  },
                  onSuggestionSelected: (suggestion) {
                    print(suggestion);
                  },
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.send),
              iconSize: 25,
              color: Theme.of(context).iconTheme.color,
              onPressed: () {
                _sendMessage(messagingClient);
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MessagingClient>(
      builder: (ctx, messagingClient, _) => Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - 120,
            width: MediaQuery.of(context).size.width,
            child: widget.conversation.messages.length > 0
                ? MessageList(
                    widget.conversation,
                    editFunction,
                    deleteFunction,
                  )
                : Center(
                    child: Text(
                      'Nothing Here! :(',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
          ),
          messagingClient.hasJoinedConversation(widget.conversation)
              ? _sendMessageArea(messagingClient)
              : _joinConversationArea(messagingClient),
        ],
      ),
    );
  }
}
