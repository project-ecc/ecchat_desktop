//@dart=2.7
import 'package:ecchat_desktop/services/daemon_client.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:clipboard/clipboard.dart';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  TextEditingController _nicknameController = TextEditingController();
  TextEditingController _routingTagController = TextEditingController();
  FocusNode _nicknameFocusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  _getStarted() async {
    _formKey.currentState.save();

    if (_nicknameController.text.isEmpty) {
      await Provider.of<MessagingClient>(context, listen: false)
          .account
          .saveUserAccount(
              _routingTagController.text, _routingTagController.text);
    } else {
      await Provider.of<MessagingClient>(context, listen: false)
          .account
          .saveUserAccount(
              _nicknameController.text, _routingTagController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/ecc_logo.png',
              height: 200,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text('Welcome to ECChat'),
              ),
            ),
            Container(
              width: 650,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    'As this is your first time using ECChat you will have to setup your nickname, this nickname can be shared with other users but you can also choose to remain anonymous and other users will see your routing tag below',
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 600,
                  child: TextFormField(
                    controller: _nicknameController,
                    focusNode: _nicknameFocusNode,
                    style: Theme.of(context).textTheme.bodyText1,
                    decoration: InputDecoration(
                      hintText: 'Nickname',
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Consumer<DaemonClient>(
                  builder: (ctx, client, _) => FutureBuilder(
                    future: client.getRoutingpubkey,
                    builder: (context, clientResult) {
                      if (clientResult.connectionState ==
                          ConnectionState.waiting) {
                        return Container(
                          child: Text('Loading'),
                        );
                      } else if (clientResult.connectionState ==
                          ConnectionState.done) {
                        _routingTagController.text =
                            clientResult.data.toString();
                        return Row(
                          children: [
                            Container(
                              width: 450,
                              child: TextFormField(
                                style: Theme.of(context).textTheme.bodyText1,
                                decoration: InputDecoration(
                                  hintText: 'Nickname',
                                ),
                                readOnly: true,
                                controller: _routingTagController,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            RaisedButton(
                              color: Theme.of(context).accentColor,
                              onPressed: () {
                                FlutterClipboard.copy(
                                        clientResult.data.toString())
                                    .then((value) {
                                  showSimpleNotification(
                                    Text(
                                      "Copied To Clipboard",
                                    ),
                                    position: NotificationPosition.bottom,
                                    background: Colors.green,
                                  );
                                });
                              },
                              child: Text('Copy to clipboard'),
                            )
                          ],
                        );
                      }
                      return Container(
                        child: Text('Cant load routing Tag'),
                      );
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    disabledColor: Theme.of(context).accentColor,
                    color: Theme.of(context).accentColor,
                    child: Text('Get Started'),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.

                        _getStarted();
                      }
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
