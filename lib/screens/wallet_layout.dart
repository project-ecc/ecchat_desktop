//@dart=2.7
import 'package:ecchat_desktop/locations/wallet_locations.dart';
import 'package:ecchat_desktop/sidebar/coin_sidebar.dart';
import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class WalletLayout extends StatefulWidget {
  final _beamerKey = GlobalKey<BeamerState>();
  @override
  _WalletLayoutState createState() => _WalletLayoutState();
}

class _WalletLayoutState extends State<WalletLayout> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Container(
            color: Color(0xff0f1a29),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: CoinSideBar(),
            ),
          ),
        ),
        Expanded(
            flex: 6,
            child: Beamer(
              key: widget._beamerKey,
              routerDelegate: BeamerRouterDelegate(
                locationBuilder: BeamerLocationBuilder(
                  beamLocations: [
                    WalletOverviewLocation(),
                    WalletSendLocation(),
                    WalletReceiveLocation(),
                    WalletTransactionLocation()
                  ],
                ),
              ),
            )),
      ],
    );
  }
}
