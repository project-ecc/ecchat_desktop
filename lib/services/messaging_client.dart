//@dart=2.7
import 'package:bitcoinrpc/bitcoinrpc.dart';
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/models/file_object.dart';
import 'package:ecchat_desktop/models/packet.dart';
import 'package:ecchat_desktop/services/account.dart';
import 'package:ecchat_desktop/services/daemon_client.dart';
import 'package:ecchat_desktop/models/message.dart';
import 'package:ecchat_desktop/models/participant.dart';
import 'package:ecchat_desktop/services/client_helper.dart';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:random_color/random_color.dart';
import 'package:uuid/uuid.dart';
import 'dart:async';
import 'dart:convert';
import 'package:convert/convert.dart';
import '../extensions.dart';
import 'dart:ffi';

class MessagingClient extends ClientHelper
    with ChangeNotifier, WidgetsBindingObserver {
  List<Message> pendingMessages = [];

  String bufferKey;
  int bufferId = 1;
  Timer registerTimer;
  Timer bufferTimer;
  bool bufferConnected = false;
  Pointer<Void> bindingSock;

  MessagingClient() {
    this.account = Account();
    this.clientList['ECC'] = DaemonClient(
      host: '127.0.0.1',
      port: 19119,
      username: 'yourusername',
      password: 'yourpassword',
    );
    WidgetsBinding.instance.addObserver(this);
    this.registerBuffer();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    this.clientList['ECC'].shutDownDaemon();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
  }

  Future<void> releaseBuffer() async {
    print('trying to release buffer');
    print(this.bufferKey);
    if (this.bufferKey != null) {
      await this.clientList['ECC'].releaseBuffer(this.bufferKey);
      if (bufferTimer.isActive) {
        bufferTimer.cancel();
      }
      bufferConnected = false;
      notifyListeners();
    }
  }

  Future<void> registerBuffer() async {
    print('trying to register buffer');
    print(this.bufferKey);
    try {
      this.bufferKey = await this.clientList['ECC'].registerBuffer;
    } catch (e) {
      print(e.toString());
    }

    if (bufferKey == null) {
      registerTimer = Timer.periodic(Duration(seconds: 60), (timer) async {
        try {
          this.bufferKey = await this.clientList['ECC'].registerBuffer;
          if (this.bufferKey != null) {
            if (timer.isActive) {
              timer.cancel();
            }
            bufferConnected = true;
            notifyListeners();
            loadBuffer();
          }
        } catch (e) {
          print(e.toString());
          if (timer.isActive) {
            timer.cancel();
          }
        }
      });
    } else {
      if (registerTimer != null && registerTimer.isActive) {
        registerTimer.cancel();
      }
      bufferConnected = true;
      notifyListeners();
      loadBuffer();
    }
  }

  Future<void> resetBufferTimeout() async {
    print('resetting buffer timeout');
    print(this.bufferKey);
    var bufferSig = await this
        .clientList['ECC']
        .bufferSignMessage(this.bufferKey, 'ResetBufferTimeout');
    try {
      await this.clientList['ECC'].resetBufferTimeout(bufferSig);
      this.bufferConnected = true;
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> loadBuffer() async {
    print(this.bufferKey);
    // register buffer on first try
    bufferTimer = Timer.periodic(Duration(seconds: 5), (timer) async {
      try {
        // reset the buffer every 5 seconds
        await resetBufferTimeout();

        // buffer command for sig
        var bufferCmd = 'GetBufferRequest:1${this.bufferId.toString()}';
        this.bufferId = this.bufferId + 1;
        var bufferSig = await this
            .clientList['ECC']
            .bufferSignMessage(this.bufferKey, bufferCmd);

        // get data from buffer

        var data = await this.clientList['ECC'].getBuffer(bufferSig);
        if (data.isNotEmpty) {
          data.forEach((key, value) {
            var bytes = hex.decode(value);
            var jsonString = utf8.decode(bytes);
            if (json.decode(jsonString) != null) {
              _processMessage(json.decode(jsonString));
            }
          });
        }
        bufferConnected = true;
      } on RPCException catch (e) {
        print(e.toString());
        if (timer.isActive) {
          timer.cancel();
          bufferConnected = false;
          await releaseBuffer();
          bufferKey = null;
          await registerBuffer();
        }
      }
    });

    // send online message to peers.

    for (Participant p in participantsBox.values) {
      Packet packet = Packet(
        to: p.routingTag,
        from: account.routingTag,
        data: jsonEncode([]),
        id: Uuid().v4(),
        meth: 'nodeon',
        ver: "1",
      );

      await this.clientList['ECC'].sendPacket(packet);
    }
  }

  void _processMessage(dynamic message) {
    this.parseMessage(message);
    notifyListeners();
  }

  Conversation findConversationById(String id) {
    if (id == null) {
      return null;
    }
    return conversationBox.values.firstWhere((element) => element.id == id);
  }

  /**
   * Conversation Logic
   */

  // Start conversation
  Future<void> startConversation(
      String routingTag, String conversationName, bool shareNick) async {
    Conversation conversation = Conversation(
      id: Uuid().v4(),
      name: conversationName.length > 0 ? conversationName : routingTag,
      createdAt: DateTime.now().toIso8601String(),
      creatorTag: account.routingTag,
      participants: HiveList(participantsBox),
      messages: HiveList(messageBox),
      ownerid: account.routingTag,
    );

    RandomColor _randomColor = RandomColor();

    Color p1Color = _randomColor.randomColor(
      colorBrightness: ColorBrightness.light,
    );

    Participant me = Participant(
      conversationId: conversation.id,
      nickName: shareNick ? account.nickname : account.routingTag,
      routingTag: account.routingTag,
      joinStatus: 'joined',
      status: 'online',
      colorHex: p1Color.toHex(leadingHashSign: false),
    );

    Color p2Color = _randomColor.randomColor(
      colorBrightness: ColorBrightness.light,
    );

    Participant them = Participant(
      conversationId: conversation.id,
      nickName: routingTag,
      routingTag: routingTag,
      joinStatus: 'pending',
      status: 'online',
      colorHex: p2Color.toHex(leadingHashSign: false),
    );

    participantsBox.add(me);
    participantsBox.add(them);

    conversation.participants.add(me);
    conversation.participants.add(them);

    Packet packet = Packet(
      to: routingTag,
      from: account.routingTag,
      data: jsonEncode(conversation),
      id: Uuid().v4(),
      meth: 'convadd',
      ver: "1",
    );

    await this.clientList['ECC'].sendPacket(packet);

    conversationBox.put(conversation.id, conversation);

    conversation.save();

    notifyListeners();
  }

  Future<void> leaveConversation(Conversation conversation) async {
    var participants = conversation.participants
        .where((element) => element.routingTag == account.routingTag);

    if (participants.first != null) {
      var me = participants.first;
      me.joinStatus = 'pending';
      me.save();

      for (Participant p in conversation.participants) {
        Packet packet = Packet(
          to: p.routingTag,
          from: account.routingTag,
          data: jsonEncode(me),
          id: Uuid().v4(),
          meth: 'partedit',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(packet);
        notifyListeners();
      }
    }
  }

  Future<void> renameConversation(
      Conversation conversation, String name) async {
    conversation.name = name;
  }

  Future<void> deleteConverstaion(Conversation conversation) async {
    await conversation.delete();
    notifyListeners();
  }

  void clearConversation(Conversation conversation) {
    conversation.messages.clear();
    notifyListeners();
  }

  Future<void> selectConversation(Conversation conversation) async {
    // check online status

    for (Participant p in conversation.participants) {
      try {
        bool haveRoute = await clientList['ECC'].haveRoute(p.routingTag);
        p.status = haveRoute ? 'online' : 'offline';
        p.save();
        if (!haveRoute) {
          await clientList['ECC'].findRoute(p.routingTag);
        }
      } catch (e) {
        print(e.toString());
        print('cant check online status');
      }
    }
    notifyListeners();
  }

  bool hasJoinedConversation(Conversation c) {
    var foundParticipant = c.participants
        .where((element) => element.routingTag == account.routingTag);
    if (foundParticipant.first != null) {
      var participant = foundParticipant.first;
      if (participant.joinStatus == 'pending') {
        return false;
      } else if (participant.joinStatus == 'joined') {
        return true;
      }
    } else {
      return false;
    }
    return false;
  }

  /**
   * Message logic
   */
  Future<void> sendMessage(Conversation c, Message m, String meth) async {
    for (Participant p in c.participants) {
      if (p.routingTag != account.routingTag) {
        Packet packet = Packet(
          to: p.routingTag,
          from: account.routingTag,
          data: jsonEncode(m),
          id: Uuid().v4(),
          meth: meth,
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(packet);
        undeliveredPacketsBox.put(packet.id, packet);
        notifyListeners();
      }
    }
  }

  Future<void> addMessageToConversation(Conversation c, Message m) async {
    messageBox.add(m);
    c.messages.add(m);
    await c.save();
    notifyListeners();
  }

  void editMessageInConversation(Conversation c, Message m) {
    try {
      var oldMessage = c.messages.where((element) => element.id == m.id).first;
      oldMessage.content = m.content;
      oldMessage.updatedAt = m.updatedAt;
      oldMessage.save();
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  void deleteMessageInConversation(Message m) {
    m.delete();
    notifyListeners();
  }

  /**
   * Participants logic
   */

  Future<void> sendParticipant(Conversation c, Participant participant) async {
    for (Participant p in c.participants) {
      Packet packet = Packet(
        to: p.routingTag,
        from: account.routingTag,
        data: jsonEncode(participant),
        id: Uuid().v4(),
        meth: 'partedit',
        ver: "1",
      );
      await this.clientList['ECC'].sendPacket(packet);
      notifyListeners();
    }
  }

  void addParticipantToConversation(Conversation c, Participant p) {
    participantsBox.add(p);
    c.participants.add(p);
    c.save();
    notifyListeners();
  }

  Future<void> reInviteParticipant(Conversation c, Participant p) async {
    Packet packet = Packet(
      to: p.routingTag,
      from: account.routingTag,
      data: jsonEncode(c),
      id: Uuid().v4(),
      meth: 'convadd',
      ver: "1",
    );

    await this.clientList['ECC'].sendPacket(packet);
  }

  void deleteParticipant(Conversation c, Participant p) {}

  Future<void> sendFile(Conversation c, FileObject f) async {
    for (Participant p in c.participants) {
      Packet packet = Packet(
        to: p.routingTag,
        from: account.routingTag,
        data: jsonEncode(f),
        id: Uuid().v4(),
        meth: 'fileadd',
        ver: "1",
      );
      await this.clientList['ECC'].sendPacket(packet);
      notifyListeners();
    }
  }
}
