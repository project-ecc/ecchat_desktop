//@dart=2.7
import 'dart:convert';

import 'package:ecchat_desktop/models/packet.dart';
import 'package:ecchat_desktop/models/wallet.dart';

import 'package:bitcoinrpc/bitcoinrpc.dart';
import 'package:ecchat_desktop/zmq/zmq.dart';
import 'package:convert/convert.dart';
import 'package:flutter/material.dart';

class DaemonClient extends ChangeNotifier {
  Wallet walletState;
  int port;
  String host;
  String username;
  String password;
  dynamic _rpcClient;

  ZmqSocket _zmqSocket;

  ZeroMQ _zmq;

  DaemonClient({this.port, this.host, this.username, this.password}) {
    _rpcClient = RPCClient(
      host: host,
      port: port,
      username: username,
      password: password,
    );

    walletState = Wallet();

    this.setUpDaemon();
  }

  Future<void> setUpDaemon() async {
    this._zmq = ZeroMQ();
    this._zmqSocket = _zmq.createSocket(SocketMode.sub);

    //filter all topics
    this._zmqSocket.filterTopic('');

    // grab zmq notifiers from daemon
    final zmqNotifiers = await _rpcClient.getzmqnotifications;
    for (final notifier in zmqNotifiers) {
      this._zmqSocket.connect(notifier['address']);
    }

    //listen to incoming events
    this._zmqSocket.messages.listen((event) async {
      try {
        if (event.topic == 'hashblock') {
          print(event.topic);
          String hash = hex.encode(event.payload);
          final info = await this._rpcClient.getinfo;
          print(hash);
          walletState.setInfo(info);
          notifyListeners();
        }
      } catch (e) {
        print(e);
      }
    });
    // ping daemon is running

    // if running ping for zmq connections

    // loop over zmq and connec to the ports.
  }

  Future<void> shutDownDaemon() {
    this._zmqSocket.close();
    this._rpcClient.stop();
  }

  //wallet functions

  //roam functions... need to be abstracted somehow

  Future<int> get bestBlockHash async {
    try {
      return await _rpcClient.getblockcount;
    } catch (e) {
      print(e.toString());
    }
    return 0;
  }

  Future<bool> sendPacket(Packet packet) async {
    return await _rpcClient.sendpacket(
      packet.to,
      1,
      jsonEncode(packet),
    );
  }

  Future<bool> haveRoute(String tagId) async {
    try {
      return await _rpcClient.haveroute(tagId);
    } catch (e) {
      print(e.toString());
    }
    return false;
  }

  Future<void> findRoute(String routingId) async {
    try {
      return await _rpcClient.findroute(routingId);
    } catch (e) {
      print(e.toString());
    }
    return false;
  }

  Future<String> get getRoutingpubkey async {
    return await _rpcClient.getroutingpubkey;
  }

  Future<Map> getBuffer(String sig) async {
    try {
      return await _rpcClient.getBuffer(1, sig);
    } catch (e) {
      rethrow;
    }
  }

  Future<String> get registerBuffer async {
    try {
      return await _rpcClient.registerBuffer(1);
    } catch (e) {
      rethrow;
    }
  }

  Future<void> releaseBuffer(String bufferKey) async {
    try {
      return await _rpcClient.releasebuffer(1, bufferKey);
    } catch (e) {
      rethrow;
    }
  }

  Future<String> bufferSignMessage(String key, String msg) async {
    try {
      return await _rpcClient.buffersignmessage(key, msg);
    } catch (e) {
      rethrow;
    }
  }

  Future<void> resetBufferTimeout(String sig) async {
    try {
      return await _rpcClient.resetbuffertimeout(1, sig);
    } catch (e) {
      rethrow;
    }
  }

  Future<String> tagSignMessage(String message) async {
    return await _rpcClient.tagSignMessage(message);
  }

  Future<bool> tagVerifyMessage(
      String routingKey, String signature, String message) async {
    return await _rpcClient.tagVerifyMessage(routingKey, signature, message);
  }
}
