//@dart=2.7
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class Account with ChangeNotifier {
  Account() {
    this.loadUserAccount();
  }
  String _nickName;
  String _routingTag;

  bool get isAuth {
    return _routingTag != null;
  }

  String get routingTag {
    return _routingTag;
  }

  String get nickname {
    return _nickName;
  }

  Future<bool> loadUserAccount() async {
    var box = await Hive.openBox('account');

    if (box.get('nickname') == null || box.get('routingTag') == null) {
      return false;
    }

    _nickName = box.get('nickname');
    _routingTag = box.get('routingTag');
    notifyListeners();
    return true;
  }

  Future<void> saveUserAccount(String nick, String tag) async {
    var box = await Hive.openBox('account');

    await box.put('nickname', nick);
    await box.put('routingTag', tag);
    _nickName = nick;
    _routingTag = tag;
    notifyListeners();
  }
}
