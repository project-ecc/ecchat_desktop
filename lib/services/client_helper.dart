//@dart=2.7
import 'dart:convert';
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/models/file_object.dart';
import 'package:ecchat_desktop/models/message.dart';
import 'package:ecchat_desktop/models/packet.dart';
import 'package:ecchat_desktop/services/account.dart';
import 'package:ecchat_desktop/services/daemon_client.dart';
import 'package:hive/hive.dart';
import 'package:ecchat_desktop/models/participant.dart';
import 'package:uuid/uuid.dart';

class ClientHelper {
  Box conversationBox = Hive.box<Conversation>('conversationBox');
  Box participantsBox = Hive.box<Participant>('participantsBox');
  Box messageBox = Hive.box<Message>('messageBox');
  Box undeliveredPacketsBox = Hive.box<Packet>('undeliveredPacketsBox');
  Map<String, DaemonClient> clientList = {};
  Account account;

  Future<void> parseMessage(dynamic message) async {
    Packet packet;
    try {
      packet = Packet.fromJson(message);
    } catch (e) {
      print(e);
    }
    if (packet == null) {
      return;
    }
    print('packet Data below');
    print(packet.data);

    switch (packet.meth) {
      case 'nodeon':
        var unsentPackets = undeliveredPacketsBox.values
            .where((element) => element.to == packet.from);
        for (Packet p in unsentPackets) {
          await this.clientList['ECC'].sendPacket(p);
        }

        break;
      case 'ping':
        break;
      case 'pong':
        break;
      case 'convadd':
        dynamic payload = jsonDecode(packet.data);

        Conversation conversation = Conversation.fromJson(payload);

        Conversation storedConversation = conversationBox.get(conversation.id);
        if (storedConversation == null) {
          conversationBox.put(conversation.id, conversation);
          conversation.save();
        }

        //
        Map<String, String> response = {
          'req_id': packet.id,
          'conv_id': conversation.id,
        };

        Packet reply = Packet(
          to: packet.from,
          from: account.routingTag,
          data: jsonEncode(response),
          id: Uuid().v4(),
          meth: 'convack',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(reply);

        break;
      case 'convedit':
        dynamic payload = jsonDecode(packet.data);
        Conversation conversation = Conversation.fromJson(payload);
        Conversation storedConversation = conversationBox.get(conversation.id);
        if (storedConversation == null) {
          return;
        }
        storedConversation.name = conversation.name;
        storedConversation.save();

        //
        Map<String, String> response = {
          'req_id': packet.id,
          'conv_id': conversation.id,
        };

        Packet reply = Packet(
          to: packet.from,
          from: account.routingTag,
          data: jsonEncode(response),
          id: Uuid().v4(),
          meth: 'convack',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(reply);

        break;
      case 'convack':
        print(packet.data.toString());

        //verify message packet was received
        Map<String, dynamic> payload = jsonDecode(packet.data);
        try {
          Conversation conversation = conversationBox.get(payload['conv_id']);
          print(conversation.toJson().toString());
          await undeliveredPacketsBox.delete(payload['req_id']);
        } catch (e) {
          print(e);
        }
        //this will acknowledge the packet was received
        break;
      case 'messadd':
        dynamic payload = jsonDecode(packet.data);
        Message message = Message.fromJson(payload);
        Conversation storedConversation =
            conversationBox.get(message.conversationId);
        if (storedConversation == null) {
          print('conversation is null messadd');
          return;
        }
        messageBox.add(message);
        storedConversation.messages.add(message);
        storedConversation.save();

        //
        Map<String, String> response = {
          'req_id': packet.id,
          'mess_id': message.id,
        };

        Packet reply = Packet(
          to: packet.from,
          from: account.routingTag,
          data: jsonEncode(response),
          id: Uuid().v4(),
          meth: 'messack',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(reply);

        break;
      case 'messedit':
        dynamic payload = jsonDecode(packet.data);
        Message message = Message.fromJson(payload);
        Conversation storedConversation =
            conversationBox.get(message.conversationId);
        if (storedConversation == null) {
          return;
        }
        var storedMessage = storedConversation.messages
            .where((element) => element.id == message.id)
            .first;
        if (storedMessage == null) {
          return;
        }
        storedMessage.content = message.content;
        storedMessage.updatedAt = message.updatedAt;
        storedMessage.save();
        storedConversation.save();

        //
        Map<String, String> response = {
          'req_id': packet.id,
          'mess_id': message.id,
          'updated_at': message.updatedAt.toUtc().toIso8601String()
        };

        Packet reply = Packet(
          to: packet.from,
          from: account.routingTag,
          data: jsonEncode(response),
          id: Uuid().v4(),
          meth: 'messack',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(reply);
        break;
      case 'messdel':
        dynamic payload = jsonDecode(packet.data);
        Message message = Message.fromJson(payload);
        Conversation storedConversation =
            conversationBox.get(message.conversationId);
        if (storedConversation == null) {
          return;
        }
        var storedMessage = storedConversation.messages
            .where((element) => element.id == message.id)
            .first;
        if (storedMessage == null) {
          return;
        }
        storedMessage.delete();
        storedConversation.save();
        break;
      case 'messack':
        //verify message packet was received
        Map<String, dynamic> payload = jsonDecode(packet.data);
        try {
          Message message = messageBox.values
              .where((element) => element.id == payload['mess_id'])
              .first;
          print(message.toJson().toString());
          await undeliveredPacketsBox.delete(payload['req_id']);
        } catch (e) {
          print(e);
        }

        break;
      case 'partadd':
        dynamic payload = jsonDecode(packet.data);
        Participant participant = Participant.fromJson(payload);
        Conversation storedConversation =
            conversationBox.get(participant.conversationId);
        if (storedConversation == null) {
          print('converation is null partadd');
          return;
        }
        participantsBox.add(participant);
        storedConversation.participants.add(participant);
        storedConversation.save();

        //
        Map<String, String> response = {
          'req_id': packet.id,
          'part_id': participant.routingTag,
        };

        Packet reply = Packet(
          to: packet.from,
          from: account.routingTag,
          data: jsonEncode(response),
          id: Uuid().v4(),
          meth: 'partack',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(reply);
        break;
      case 'partedit':
        dynamic payload = jsonDecode(packet.data);
        Participant participant = Participant.fromJson(payload);
        Conversation storedConversation =
            conversationBox.get(participant.conversationId);
        if (storedConversation == null) {
          print('converation is null partedit');
          return;
        }
        var storedParticipant = storedConversation.participants
            .where((element) => element.routingTag == participant.routingTag)
            .first;
        if (storedParticipant == null) {
          return;
        }
        storedParticipant.joinStatus = participant.joinStatus;
        storedParticipant.nickName = participant.nickName;
        storedParticipant.status = participant.status;
        storedParticipant.save();
        storedConversation.save();

        //
        Map<String, String> response = {
          'req_id': packet.id,
          'part_id': participant.routingTag,
        };

        Packet reply = Packet(
          to: packet.from,
          from: account.routingTag,
          data: jsonEncode(response),
          id: Uuid().v4(),
          meth: 'partack',
          ver: "1",
        );
        await this.clientList['ECC'].sendPacket(reply);
        break;
      case 'partack':
        print(packet.data.toString());
        //acknowledge participant packet was received

        //verify message packet was received
        Map<String, dynamic> payload = jsonDecode(packet.data);
        try {
          Participant participant = participantsBox.values
              .where((element) => element.routingTag == payload['part_id'])
              .first;
          print(participant.toJson().toString());
          await undeliveredPacketsBox.delete(payload['req_id']);
        } catch (e) {
          print(e);
        }
        //this will acknowledge the packet was received
        break;
      case 'fileadd':
        dynamic payload = jsonDecode(packet.data);
        FileObject fileObject = FileObject.fromJson(payload);
        var storedConversation = conversationBox.values
            .where((element) => element.id == fileObject.conversationId)
            .first;
        if (storedConversation == null) {
          return;
        }
        var storedMessage = storedConversation.messages
            .where((element) => element.id == fileObject.messageId)
            .first;
        if (storedMessage == null) {
          return;
        }

        // store file in local system storage and add path to media object.
        storedMessage.content = message.content;
        storedMessage.updatedAt = message.updatedAt;
        break;
    }
  }
}
