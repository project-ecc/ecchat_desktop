//@dart=2.7
import 'package:beamer/beamer.dart';
import 'package:ecchat_desktop/locations/home_location.dart';
import 'package:ecchat_desktop/locations/start_location.dart';
import 'package:ecchat_desktop/models/conversation.dart';
import 'package:ecchat_desktop/models/packet.dart';
import 'package:ecchat_desktop/models/participant.dart';
import 'package:ecchat_desktop/services/messaging_client.dart';
import 'package:ecchat_desktop/models/message.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'dart:io';

Socket socket;
void main() async {
  Provider.debugCheckInvalidValueType = null;
  WidgetsFlutterBinding.ensureInitialized();

  final appDirectory = await path.getApplicationDocumentsDirectory();
  await Hive.initFlutter(appDirectory.path);
  Hive.registerAdapter(PacketAdapter());
  Hive.registerAdapter(MessageAdapter());
  Hive.registerAdapter(ConversationAdapter());
  Hive.registerAdapter(ParticipantAdapter());
  await Hive.openBox<Participant>('participantsBox');
  await Hive.openBox<Message>('messageBox');
  await Hive.openBox<Conversation>('conversationBox');
  await Hive.openBox<Packet>('undeliveredPacketsBox');

  runApp(MyApp());

  // await DesktopWindow.setWindowSize(Size(1024, 768));

  // await DesktopWindow.setMinWindowSize(Size(1024, 768));
}

class MyApp extends StatelessWidget {
  final BeamLocation initialLocation = HomeLocation();

  final routerDelegate = RootRouterDelegate(
    guards: [
      BeamGuard(
        pathBlueprints: ['/start'],
        guardNonMatching: true,
        check: (context, location) {
          print(Provider.of<MessagingClient>(context).account.isAuth);
          return true;
        },
        onCheckFailed: (context, location) => print('failed $location'),
        beamTo: (context) => StartLocation(),
      )
    ],
    locationBuilder: BeamerLocationBuilder(
      beamLocations: [
        HomeLocation(),
        StartLocation(),
      ],
    ),
  );

  final notFoundPage = BeamPage(
    child: Scaffold(
      body: Center(
        child: Text('Not found'),
      ),
    ),
  );
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => MessagingClient()),
        ],
        child: Builder(
          builder: (context) => MaterialApp.router(
            routerDelegate: routerDelegate,
            routeInformationParser: BeamerRouteInformationParser(),
            backButtonDispatcher:
                BeamerBackButtonDispatcher(delegate: routerDelegate),
            debugShowCheckedModeBanner: false,
            title: 'ECC Chat',
            theme: ThemeData(
              unselectedWidgetColor: Colors.orangeAccent,
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              primaryColor: const Color(0xff1c1c23),
              accentColor: Colors.orangeAccent,
              hintColor: Color(0xffcecece),
              iconTheme: IconThemeData(
                color: Color(0xffcecece),
              ),
              canvasColor: const Color(0xff292c2d),
              // This makes the visual density adapt to the platform that you run
              // the app on. For desktop platforms, the controls will be smaller and
              // closer together (more dense) than on mobile platforms.
              visualDensity: VisualDensity.adaptivePlatformDensity,
              textTheme: TextTheme(
                bodyText1: TextStyle(
                  fontSize: 16,
                  fontFamily: 'Raleway',
                  color: Color(0xffcecece),
                ),
                bodyText2: TextStyle(
                  fontSize: 14,
                  fontFamily: 'Raleway',
                  color: Color(0xffcecece),
                ),
                subtitle2: TextStyle(
                  fontSize: 10,
                  fontFamily: 'Raleway',
                  color: Color(0xffcecece).withOpacity(.8),
                ),
                headline1: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Raleway',
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
                headline6: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Raleway',
                  fontWeight: FontWeight.bold,
                  color: Color(0xffcecece),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
