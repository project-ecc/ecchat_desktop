//@dart=2.7
import 'package:beamer/beamer.dart';
import 'package:ecchat_desktop/screens/wallet/overview.dart';
import 'package:ecchat_desktop/screens/wallet/receive.dart';
import 'package:ecchat_desktop/screens/wallet/send.dart';
import 'package:ecchat_desktop/screens/wallet/transactions.dart';
import 'package:ecchat_desktop/screens/wallet_layout.dart';
import 'package:flutter/material.dart';

class WalletRootLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('wallet'),
        child: WalletLayout(),
      )
    ];
  }

  @override
  List<String> get pathBlueprints => ['/wallet/*'];
}

class WalletOverviewLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('wallet-overview'),
        child: Overview(),
      ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/wallet/overview'];
}

class WalletSendLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('wallet-send'),
        child: Send(),
      ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/wallet/send'];
}

class WalletReceiveLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('wallet-receive'),
        child: Receive(),
      ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/wallet/receive'];
}

class WalletTransactionLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('wallet-transactions'),
        child: Transactions(),
      ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/wallet/transactions'];
}
