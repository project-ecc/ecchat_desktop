//@dart=2.7
import 'package:ecchat_desktop/screens/messaging_layout.dart';
import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class MessageLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) => [
        BeamPage(
          key: ValueKey('messaging'),
          child: MessagingLayout(),
        ),
      ];

  @override
  List<String> get pathBlueprints => [
        '/messaging/*',
      ];
}
