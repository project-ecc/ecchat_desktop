import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';

class ChainLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) => [
        BeamPage(
          key: ValueKey('chains'),
          child: Center(
            child: Text('HI'),
          ),
        )
      ];

  @override
  List<String> get pathBlueprints => ['/main/chains/*'];
}
