//@dart=2.7
import 'package:ecchat_desktop/sidebar/main_layout.dart';
import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class HomeLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('home-${UniqueKey()}'),
        child: MainLayout(),
      ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/*', '/wallet/overview'];
}
