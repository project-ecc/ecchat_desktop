//@dart=2.7
import 'package:ecchat_desktop/screens/start_screen.dart';
import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class StartLocation extends BeamLocation {
  // StartLocation() : super(state: BeamState(pathBlueprintSegments: ['start']));
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('Start Screen'),
        child: StartScreen(),
      ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/start'];
}
