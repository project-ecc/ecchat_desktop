//@dart=2.7
import 'package:ecchat_desktop/screens/messaging/chat_screen.dart';
import 'package:ecchat_desktop/screens/messaging/select_conversation_screen.dart';
import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class ConversationLocation extends BeamLocation {
  @override
  List<BeamPage> pagesBuilder(BuildContext context, BeamState state) {
    return [
      BeamPage(
        key: ValueKey('conversation-select'),
        child: SelectConversationScreen(),
      ),
      if (state.pathParameters.containsKey('conversationId'))
        BeamPage(
          key: ValueKey(
              'conversation-${state.pathParameters['conversationId']}'),
          child: ChatScreen(
            conversationId: state.pathParameters['conversationId'],
          ),
        ),
    ];
  }

  @override
  List<String> get pathBlueprints => ['/messaging/:conversationId'];
}
